<?php
class User_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_user_by_email($email)
    {
        $query = $this->db->get_where('uporabnik', array('mail' => $email));
        return $query->row();
    }

}