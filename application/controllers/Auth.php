<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->helper('url_helper');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->helper('form');
    }

    public function index(){
        if($this->session->logged_in){
            redirect('welcome');
        }else {
            $this->load->view('auth/login');
        }
    }

    public function login(){


        $this->load->view('header');
        $this->load->view('auth/login');
        $this->load->view('footer');
    }

    public function proceed_login(){
        //validate form
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Geslo', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('header');
            $this->load->view('auth/login');
            $this->load->view('footer');

        }else {

            // get user from db
            $user = $this->user_model->get_user_by_email($this->input->post('email'));

            // check if correct password
            if ($user && hash('sha512', $this->input->post('password')) == $user->geslo) {
                $userdata = array(
                    'email' => $user->mail,
                    'logged_in' => TRUE
                );
                // add user data to session
                $this->session->set_userdata($userdata);
                redirect('welcome');
            }else{
                $this->load->view('header');
                $this->load->view('auth/login');
                $this->load->view('footer');
            }
        }

    }


    public function logout(){
        // clear current session
        $this->session->sess_destroy();
        redirect('login');
    }

}